# -*- coding: utf-8 -*-
# !/usr/bin/python

""" abfab_entity.py: Defines an abtract ABFAB Entity """

from xml.etree.ElementTree import ParseError

from saml2 import saml

__author__ = "Alejandro Pérez <alex@um.es>"


class AbfabEntity(object):
    def __init__(self):
        self._attribute_filters = []

    def _get_radius_attribute(self, p, attribute_name):
        try:
            return next(value for name, value in p if name == attribute_name)
        except StopIteration:
            return None

    def _get_radius_attributes(self, p, attribute_name):
        attributes = [value for name, value in p if name == attribute_name]
        if len(attributes) == 0:
            return None
        return ''.join(attributes)

    def _get_acceptor_name(self, p):
        acceptor_service_name = self._get_radius_attribute(p, 'GSS-Acceptor-Service-Name')
        acceptor_host_name = self._get_radius_attribute(p, 'GSS-Acceptor-Host-Name')

        acceptor_name = ''
        if acceptor_service_name:
            acceptor_name = str(acceptor_service_name)
        if acceptor_host_name:
            acceptor_name += '@' + str(acceptor_host_name)
        return acceptor_name

    def _assertion_to_reply(self, assertion):
        max_rad_attr_len = 220
        assertion_str = str(assertion)
        return tuple(('SAML-AAA-Assertion', '+=', assertion_str[i:i + max_rad_attr_len])
                     for i in range(0, len(assertion_str), max_rad_attr_len))

    def _reply_to_assertion(self, p):
        # Parse assertion. In case of problems, return an empty Assertion
        assertion_attributes = self._get_radius_attributes(p, 'SAML-AAA-Assertion')
        if assertion_attributes is not None:
            assertion_str = ''.join(assertion_attributes)
            try:
                assertion = saml.assertion_from_string(assertion_str)
                if assertion is not None:
                    return assertion
            except ParseError:
                pass
        return saml.Assertion()

    def _attributes_to_statement(self, attributes):
        statement = saml.AttributeStatement()
        for name, name_format, values in attributes:
            saml_attribute = saml.Attribute(name=name, name_format=name_format)
            # add attribute values
            for value in values:
                # convert to base64 if not in utf8
                try:
                    unicode(value)
                except (UnicodeDecodeError, TypeError):
                    value = value.encode('base64')
                saml_attribute.attribute_value.append(saml.AttributeValue(text=value))
            statement.attribute.append(saml_attribute)
        return statement

    def _assertion_to_attributes(self, assertion):
        attributes = []
        for statement in assertion.attribute_statement:
            for attribute in statement.attribute:
                attribute = attribute.name, attribute.name_format, [str(x.text) for x in
                                                                    attribute.attribute_value]
                attributes.append(attribute)
        return attributes

    def _perform_filters(self, attributes, rp_name, idp_name):
        for attr_filter in self._attribute_filters:
            attributes = attr_filter.filter_attributes(attributes, rp_name=rp_name,
                                                       idp_name=idp_name)
        return attributes
