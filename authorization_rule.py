# -*- coding: utf-8 -*-
# !/usr/bin/python

""" authorization_rule.py: Defines an Authorization Rule """

import re

import radiusd

__author__ = "Alejandro Pérez <alex@um.es>"


class AuthorizationRule(object):
    def __init__(self, configuration):
        """ Creates a new Authorization Rule
        """
        self.name = configuration.get('name', '.*')
        self.nameformat = configuration.get('nameformat', '.*')
        self.value = configuration.get('value', '.*')

    def is_authorized(self, attributes):
        for name, namefmt, values in attributes:
            radiusd.radlog(radiusd.L_DBG, 'Trying to match "{0}={1}" with "{2}={3}"'
                                          ''.format(name, values, self.name, self.value))
            if (re.match(self.name, name)
                    and re.match(self.nameformat, namefmt)
                    and any(re.match(self.value, x) for x in values)):
                return True
        return False
