# -*- coding: utf-8 -*-
# !/usr/bin/python

""" abfab_rpp.py: Defines an ABFAB RPP """

import json
import sys
import traceback

import radiusd
from abfab_entity import AbfabEntity
from attribute_filters import (attribute_filter_modules)
from authorization_rule import AuthorizationRule

__author__ = "Alejandro Pérez <alex@um.es>"

global_rpp = None


class AbfabRPPError(Exception):
    pass


class AbfabRPP(AbfabEntity):
    def __init__(self, configuration):
        """ Initializes the AbfabRPP
        """
        AbfabEntity.__init__(self)
        # create attribute filters
        self._attribute_filters = []
        for filter_conf in configuration.get('attribute_filters', []):
            filter_type = filter_conf.get('type', 'Not provided')
            try:
                attribute_filter = attribute_filter_modules[filter_type](filter_conf)
                self._attribute_filters.append(attribute_filter)
            except KeyError as e:
                raise AbfabRPPError('Could not instantiate the atttribute filter: {0}'.format(e))

        # create authorization rules
        self._authorization_rules = []
        for authorization_rule_conf in configuration.get('authorization_rules', []):
            authorization_rule = AuthorizationRule(authorization_rule_conf)
            self._authorization_rules.append(authorization_rule)

    def _perform_authorization(self, attributes):
        for authorization_rule in self._authorization_rules:
            if not authorization_rule.is_authorized(attributes):
                radiusd.radlog(radiusd.L_ERR,
                               'Missing required attribute "{0}"" with value "{1}"'
                               ''.format(authorization_rule.name, authorization_rule.value))
                return False
        return True

    def post_auth(self, p):
        # convert reply to Assertion object
        request, reply, config, state, proxy_request, proxy_reply = p

        # If there is no proxy reply, it means this is not a proxied package that needs handling
        if not proxy_reply:
            return radiusd.RLM_MODULE_NOOP

        assertion = self._reply_to_assertion(proxy_reply)

        # transform the Assertion into a collection of (name, name_format, [values]) tuples
        attributes = self._assertion_to_attributes(assertion)

        # Transform attributes according to the filters
        try:
            username = self._get_radius_attribute(request, 'User-Name')
            realm = username.split('@')[-1]
        except AttributeError:
            realm = ''
        attributes = self._perform_filters(attributes, rp_name='', idp_name=realm)

        # remove all the old SAML-AAA-Assertion from the reply
        reply = (('SAML-AAA-Assertion', '!*', 'ANY'),)

        # check authorization
        if self._perform_authorization(attributes):
            assertion.attribute_statement = self._attributes_to_statement(attributes)
            # add the modified SAML-AAA-Assertion
            reply += self._assertion_to_reply(assertion)
            return radiusd.RLM_MODULE_OK, reply, None
        else:
            radiusd.radlog(radiusd.L_ERR, 'SAML authorization failed!')
            return radiusd.RLM_MODULE_FAIL, reply, None


# FreeRadius' module callbacks
def instantiate(p):
    global global_rpp
    """ Instantiates the module. This just tries to parse the configuration
    """
    try:
        with open('/etc/abfab_rpp.conf', 'r') as f:
            configuration = json.loads(f.read())
            global_rpp = AbfabRPP(configuration)
    except IOError as e:
        radiusd.radlog(radiusd.L_ERR,
                       '/etc/abfab_rpp.conf JSON configuration file missing: {0}'.format(e))
        return -1
    except ValueError as e:
        radiusd.radlog(radiusd.L_ERR,
                       '/etc/abfab_rpp.conf JSON configuration file error: {0}'.format(e))
        return -1
    except AbfabRPPError as e:
        radiusd.radlog(radiusd.L_ERR, 'Error instantiating ABFAB RPP: {0}'.format(e))
        return -1
    except Exception as e:
        radiusd.radlog(radiusd.L_ERR, 'ABFAB RPP uncaught exception: {0}'.format(e))
        traceback.print_exc(file=sys.stdout)
        return -1
    return 0


def post_auth(p):
    try:
        return global_rpp.post_auth(p)
    except AbfabRPPError as e:
        radiusd.radlog(radiusd.L_ERR, 'Error instantiating ABFAB RPP: {0}'.format(e))
        return radiusd.RLM_MODULE_FAIL
    except Exception as e:
        radiusd.radlog(radiusd.L_ERR, 'ABFAB RPP uncaught exception: {0}'.format(e))
        traceback.print_exc(file=sys.stdout)
        return radiusd.RLM_MODULE_FAIL
