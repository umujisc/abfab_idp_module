# -*- coding: utf-8 -*-
# !/usr/bin/python

""" Python module attribute_source file """

import os
import sqlite3

import ldap
import ldap.schema
import radiusd

__author__ = "Alejandro Pérez <alex@um.es>"

from saml2 import saml

class AttributeSourceError(Exception):
    pass


class AttributeSource(object):
    def __init__(self, name, attributeconf):
        self.name = name

    def retrieve_attributes(self, username, rp_name):
        """ Retrieves attributes from the attribute source, matching the
            indicated restrictions. Attribute Release Policies are applied when configured

            :param username: name of the user
            :param rp_name: name of the RP asking for the attributes
            :returns: List of tuples (attribute_name, attribute_name_format, [values])
        """
        attributes = self._retrieve_attributes(username)

        radiusd.radlog(radiusd.L_INFO,
                       'Attribute source [{0}] returned attributes for user [{1}] and RP [{2}]: [{3}]'
                       ''.format(self.name, username, rp_name, ', '.join([x[0] for x in attributes])))
        return attributes

    def _retrieve_attributes(self, username):
        """ Retrieves all the attributes from the attribute source

            :param username: name of the user
            :returns: List of tuples (attribute_name, attribute_name_format, [values])
        """
        raise NotImplementedError


class SQLiteAttributeSource(AttributeSource):
    def __init__(self, attributeconf):
        try:
            super(SQLiteAttributeSource, self).__init__('sqlite', attributeconf)
            dbpath = attributeconf['sqlite_db']
        except KeyError as e:
            raise AttributeSourceError('Attribute source [{0}] lacks required parameter: {1}'
                                       ''.format(self.name, e))
        if not os.path.isfile(dbpath):
            raise AttributeSourceError('SQLite database {0} does not exist'.format(dbpath))
        try:
            self._conn = sqlite3.connect(dbpath)
            self._conn.text_factory = str
        except sqlite3.Error:
            raise AttributeSourceError('SQLite error: {0}'.format(dbpath))

    def _retrieve_attributes(self, username):
        try:
            c = self._conn.cursor()

            query_string = ('''SELECT username, name, name_format, value
                               FROM attributes
                               WHERE username=?''')
            query_params = [username]

            c.execute(query_string, query_params)
            data = c.fetchall()
            attributes = [(row[1], row[2], [row[3]]) for row in data]
            return attributes
        except sqlite3.Error as e:
            raise AttributeSourceError('SQLite error: {0}'.format(e))


class LDAPAttributeSource(AttributeSource):
    def __init__(self, attributeconf):
        try:
            super(LDAPAttributeSource, self).__init__('ldap', attributeconf)
            self._server = attributeconf['ldap_uri']
            self._login = attributeconf.get('ldap_login')
            self._password = attributeconf.get('ldap_password')
            self._basedn = attributeconf['ldap_basedn']
            self._mapping = attributeconf['ldap_mapping']
            self._timeout = int(attributeconf.get('ldap_timeout', '2'))
        except KeyError as e:
            raise AttributeSourceError('Attribute source [{0}] lacks required parameter: {1}'
                                       ''.format(self.name, e))

    def _retrieve_attributes(self, username):
        try:
            ldapobj = ldap.initialize(self._server)
            ldapobj.protocol_version = ldap.VERSION3
            ldapobj.set_option(ldap.OPT_TIMEOUT, self._timeout)
            ldapobj.set_option(ldap.OPT_NETWORK_TIMEOUT, self._timeout)
            if self._login and self._password:
                ldapobj.simple_bind_s(self._login, self._password)
            ldapresults = ldapobj.search_ext_s(self._basedn, ldap.SCOPE_SUBTREE,
                                               self._mapping.replace('%u', username),
                                               None, sizelimit=1)
            subschema = ldapobj.search_s('cn=subschema', ldap.SCOPE_BASE, '(objectclass=*)', ['*','+'])[0][1]
            subschema = ldap.schema.SubSchema(subschema)

        except ldap.LDAPError as e:
            raise AttributeSourceError('LDAP error: {0}'.format(e))

        # if no results, return empty attribute list
        if len(ldapresults) < 1:
            return []

        # if more than one result, take the first one and print a WARNING
        if len(ldapresults) > 1:
            radiusd.radlog(radiusd.L_WARN,
                           'Source [{0}] produced more than one LDAP result for [{1}]. Taking the '
                           'first one and ignoring the rest of them'.format(self.name, username))

        return ([('urn:oid:{0}'.format(subschema.get_obj(ldap.schema.AttributeType, name).oid),
                  saml.NAME_FORMAT_URI, values)
                 for name, values in ldapresults[0][1].items()])

class StaticAttributeSource(AttributeSource):
    def __init__(self, attributeconf):
        try:
            super(StaticAttributeSource, self).__init__('static', attributeconf)
            self._attr_name = attributeconf['static_name']
            self._attr_name_format = attributeconf.get('static_name_format',
                                                       saml.NAME_FORMAT_UNSPECIFIED)
            self._value = attributeconf['static_value']
        except KeyError as e:
            raise AttributeSourceError('Attribute source [{0}] lacks required parameter: {1}'
                                       ''.format(self.name, e))

    def _retrieve_attributes(self, username):
            return [(self._attr_name, self._attr_name_format, [self._value])]

attribute_source_modules = {
    'sqlite': SQLiteAttributeSource,
    'ldap': LDAPAttributeSource,
    'static': StaticAttributeSource
}

