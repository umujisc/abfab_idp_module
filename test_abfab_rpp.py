# -*- coding: utf-8 -*-
# !/usr/bin/python

import unittest

from saml2 import saml

import radiusd
from abfab_rpp import AbfabRPP

__author__ = "Alejandro Pérez <alex@um.es>"


class TestAbfabRPP(unittest.TestCase):
    def setUp(self):
        assertion_str = '''
            <ns0:Assertion xmlns:ns0="urn:oasis:names:tc:SAML:2.0:assertion" ID="120957eb-d75a-44fa-83e9-95f0bad28542"
                    IssueInstant="2017-09-23T09:28:03Z" Version="2.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                    <ns0:Issuer>http://testrealm.org/</ns0:Issuer>
                    <ns0:Subject>
                        <ns0:NameID Format="urn:oasis:names:tc:SAML:2.0:nameid-format:transient">
                            3983e0a2-351f-4742-9bc8-11311daed023
                        </ns0:NameID>
                        <ns0:SubjectConfirmation Method="urn:ietf:params:abfab:cm:user"/>
                    </ns0:Subject>
                    <ns0:Conditions NotBefore="2017-09-23T09:28:03Z" NotOnOrAfter="2017-09-23T10:28:03Z"/>
                    <ns0:AttributeStatement>
                        <ns0:Attribute Name="idp_software" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
                            <ns0:AttributeValue xsi:type="xs:string">abfab_idp</ns0:AttributeValue>
                        </ns0:Attribute>
                        <ns0:Attribute Name="urn:oid:2.5.4.3" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                            <ns0:AttributeValue xsi:type="xs:string">ALEJANDRO PEREZ MENDEZ</ns0:AttributeValue>
                        </ns0:Attribute>
                        <ns0:Attribute Name="urn:oid:2.5.4.20" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                            <ns0:AttributeValue xsi:type="xs:string">+34 868884644</ns0:AttributeValue>
                        </ns0:Attribute>
                        <ns0:Attribute Name="urn:oid:2.5.4.0" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                            <ns0:AttributeValue xsi:type="xs:string">top</ns0:AttributeValue>
                            <ns0:AttributeValue xsi:type="xs:string">irisInetEntity</ns0:AttributeValue>
                            <ns0:AttributeValue xsi:type="xs:string">irisPerson</ns0:AttributeValue>
                            <ns0:AttributeValue xsi:type="xs:string">organizationalUnit</ns0:AttributeValue>
                            <ns0:AttributeValue xsi:type="xs:string">uidObject</ns0:AttributeValue>
                            <ns0:AttributeValue xsi:type="xs:string">extensibleObject</ns0:AttributeValue>
                            <ns0:AttributeValue xsi:type="xs:string">pkiUser</ns0:AttributeValue>
                            <ns0:AttributeValue xsi:type="xs:string">umPerson</ns0:AttributeValue>
                        </ns0:Attribute>
                        <ns0:Attribute Name="urn:oid:0.9.2342.19200300.100.1.3" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                            <ns0:AttributeValue xsi:type="xs:string">alex@um.es</ns0:AttributeValue>
                        </ns0:Attribute>
                    </ns0:AttributeStatement>
                </ns0:Assertion>'''
        self.assertion = saml.assertion_from_string(assertion_str)
        self.reply = tuple(('SAML-AAA-Assertion', assertion_str[i:i + 200])
                           for i in range(0, len(assertion_str), 200))

    def test_postauth_failed(self):
        rpp = AbfabRPP({
            'attribute_filters': [
                {
                    'type': 'combine',
                    'sources': ['cn', 'idp_software'],
                    'template': '{0} {1}',
                    'destination': 'super'
                }
            ],
            'authorization_rules': [
                {
                    'name': 'super'
                },
                {
                    'name': 'objectClass',
                    'value': 'irisPerson'
                },
                {
                    'name': 'telephoneNumber',
                    'value': 'irisPerson'
                },
            ],
        })

        result = rpp.post_auth([(), (), (), (), (), self.reply])
        self.assertEqual(result[0], radiusd.RLM_MODULE_FAIL)


if __name__ == '__main__':
    unittest.main()
