from setuptools import setup
import os.path
import sys

install_requires = [
    # core dependencies
    'pysaml2>=3',
    'python-ldap',
    'setuptools',
]

# Try to locate FreeRadius installation
raddb_folders = ['/etc/raddb', '/etc/freeradius', '/usr/local/etc/raddb']
try:
    raddb_folder = next(folder for folder in raddb_folders if os.path.isdir(folder))
except StopIteration:
    print('###################')
    print('ERROR. A valid FreeRadius installation was not found in any of the configured'
          'locations: [{0}]'.format(', '.join(raddb_folders)))
    print('Please, check you have installed it properly.')
    print('###################')
    sys.exit(1)

setup(name='abfab_idp',
      version='0.2',
      description='ABFAB IDP/RP for FreeRadius',
      author='Alejandro Perez',
      author_email='alex@um.es',
      packages=['abfab_idp'],
      package_dir={'abfab_idp': '.'},
      install_requires=install_requires,
      data_files=[
        ('/usr/share/abfab_idp/', ['abfab_idp.conf.example', 'abfab_rpp.conf.example']),
        ('{0}/mods-available/'.format(raddb_folder), ['abfab_idp', 'abfab_rpp'])])

print('''
#############################################################################
Installation success. Configuration files have been installed in {0}.
Some example configuration files files have been installed in
/usr/share/abfab_idp/. You can use them as a template for your set up.
#############################################################################
'''.format(raddb_folder))
