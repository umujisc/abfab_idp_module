# -*- coding: utf-8 -*-
# !/usr/bin/python

import unittest

import attribute_filters

__author__ = "Alejandro Pérez <alex@um.es>"


class TestBlacklistAttributeFilter(unittest.TestCase):
    def setUp(self):

        self.attributes = [
            ('attribute1', 'format1', ['value1', 'value2']),
            ('attribute2', 'format1', ['value1']),
            ('attribute3', 'format2', []),
        ]
        self.rp_name = 'service@host'
        self.username = 'alex@um.es'

    def test_emtpy_conf(self):
        attribute_filters.BlacklistAttributeFilter({})

    def test_whitelist_wins_blacklist(self):
        conf = {
            "rp_name": ".*service.*",
            "exclude": [".*"],
            "include": ["attribute2", "another"]
        }
        af = attribute_filters.BlacklistAttributeFilter(conf)
        filtered = af.filter_attributes(self.attributes, rp_name=self.rp_name)
        self.assertEqual(filtered, [
            ('attribute2', 'format1', ['value1']),
        ])

    def test_emtpy_whitelist(self):
        conf = {
            "rp_name": ".*service.*",
            "exclude": ["attribute2"],
            "include": []
        }

        af = attribute_filters.BlacklistAttributeFilter(conf)
        filtered = af.filter_attributes(self.attributes, rp_name=self.rp_name)
        self.assertEqual(filtered, [
            ('attribute1', 'format1', ['value1', 'value2']),
            ('attribute3', 'format2', []),
        ])

    def test_emtpy_blacklist(self):
        conf = {
            "rp_name": ".*service.*",
            "exclude": [],
            "include": ["attribute1"]
        }

        af = attribute_filters.BlacklistAttributeFilter(conf)
        filtered = af.filter_attributes(self.attributes, rp_name=self.rp_name)
        self.assertEqual(filtered, [
            ('attribute1', 'format1', ['value1', 'value2']),
            ('attribute2', 'format1', ['value1']),
            ('attribute3', 'format2', []),
        ])

    def test_missing_blacklist(self):
        conf = {
            "rp_name": ".*service.*",
            "include": ["attribute1"]
        }

        af = attribute_filters.BlacklistAttributeFilter(conf)
        filtered = af.filter_attributes(self.attributes, rp_name=self.rp_name)
        self.assertEqual(filtered, [
            ('attribute1', 'format1', ['value1', 'value2']),
            ('attribute2', 'format1', ['value1']),
            ('attribute3', 'format2', []),
        ])

    def test_missing_whitelist(self):
        conf = {
            "rp_name": ".*service.*",
            "exclude": ["attribute1"]
        }

        af = attribute_filters.BlacklistAttributeFilter(conf)
        filtered = af.filter_attributes(self.attributes, rp_name=self.rp_name)
        self.assertEqual(filtered, [
            ('attribute2', 'format1', ['value1']),
            ('attribute3', 'format2', []),
        ])

    def test_no_policy_found(self):
        conf1 = {
            "rp_name": ".*@test.org",
            "exclude": ["urn:.*"],
            "include": [".*entitlement.*"]
        }
        conf2 = {
            "rp_name": "another",
            "exclude": ["attribute1"]
        }

        af1 = attribute_filters.BlacklistAttributeFilter(conf1)
        af2 = attribute_filters.BlacklistAttributeFilter(conf2)
        filtered = af1.filter_attributes(self.attributes, rp_name=self.rp_name)
        filtered = af2.filter_attributes(filtered, rp_name=self.rp_name)
        self.assertEqual(filtered, [
            ('attribute1', 'format1', ['value1', 'value2']),
            ('attribute2', 'format1', ['value1']),
            ('attribute3', 'format2', []),
        ])

    def test_more_than_one_policy_found(self):
        conf1 = {
            "rp_name": ".*",
            "exclude": [".*"],
            "include": ["attribute1"]
        }
        conf2 = {
            "rp_name": "service",
            "include": ["attribute2"]
        }

        af1 = attribute_filters.BlacklistAttributeFilter(conf1)
        af2 = attribute_filters.BlacklistAttributeFilter(conf2)

        filtered = af1.filter_attributes(self.attributes, rp_name=self.rp_name)
        filtered = af2.filter_attributes(filtered, rp_name=self.rp_name)
        self.assertEqual(filtered, [
            ('attribute1', 'format1', ['value1', 'value2']),
        ])


class TestTemplateAttributeFilter(unittest.TestCase):
    def setUp(self):
        self.attributes = [
            ('attribute1', 'format1', ['value1', 'value2']),
            ('attribute2', 'format1', ['ALEJANDRO PEREZ MENDEZ']),
            ('attribute3', 'format2', []),
        ]

    def test_template(self):
        taf = attribute_filters.TemplateAttributeFilter({
            'source': 'attribute2',
            're': '(.*) (.*) .*',
            'template': '{1}, {0}',
            'destination': 'reversed_name'
        })
        attributes = taf.filter_attributes(self.attributes)
        self.assertEqual(len(attributes), 4)
        # means: fourth attribute, values, first value
        self.assertEqual(attributes[3][2][0], 'PEREZ, ALEJANDRO')

    def test_template_does_not_match(self):
        taf = attribute_filters.TemplateAttributeFilter({
            'source': 'attribute2',
            're': '(.*) AAA',
            'template': '{1}, {0}',
            'destination': 'reversed_name'
        })
        attributes = taf.filter_attributes(self.attributes)
        self.assertEqual(len(attributes), 3)

    def test_template_invalid_template(self):
        taf = attribute_filters.TemplateAttributeFilter({
            'source': 'cn',
            're': '(.*) (.*) .*',
            'template': '{1}, {10}',
            'destination': 'reversed_name'
        })
        attributes = taf.filter_attributes(self.attributes)
        self.assertEqual(len(attributes), 3)


class TestCombimeAttributeFilter(unittest.TestCase):
    def setUp(self):
        self.attributes = [
            ('attribute1', 'format1', ['value1', 'value2']),
            ('attribute2', 'format1', ['ALEJANDRO PEREZ MENDEZ']),
            ('attribute3', 'format2', []),
        ]

    def test_combine(self):
        caf = attribute_filters.CombineAttributeFilter({
            'sources': ['attribute1', 'attribute2'],
            'template': '{1} {0}',
        })
        attributes = caf.filter_attributes(self.attributes)
        self.assertEqual(len(attributes), 4)
        # means: fourth attribute, values, first value
        self.assertEqual(attributes[3][2][0], 'ALEJANDRO PEREZ MENDEZ value1')


if __name__ == '__main__':
    unittest.main()
