# -*- coding: utf-8 -*-
# !/usr/bin/python

""" abfab_idp.py: Defines an ABFAB IdP """

import collections
import datetime
import json
import sys
import traceback
import uuid

from saml2 import saml

import radiusd
from abfab_entity import AbfabEntity
from attribute_filters import (AttributeFilterError, attribute_filter_modules)
from attribute_sources import (AttributeSourceError, attribute_source_modules)

__author__ = "Alejandro Pérez <alex@um.es>"

global_idp = None


class AbfabIDPError(Exception):
    pass


class AbfabIDP(AbfabEntity):
    def __init__(self, idpconf):
        """ Initializes the AbfabIDP
            :param idpconf: IDP configuration (as a dict)
        """
        AbfabEntity.__init__(self)
        self._issuer = idpconf.get('assertion_issuer', 'https://abfab_idp')
        self._lifetime = int(idpconf.get('assertion_lifetime', '60'))

        # create the attribute sources
        self._attribute_sources = []
        for attributeconf in idpconf.get('attribute_sources', []):
            att_type = attributeconf['type']
            try:
                attribute_source = attribute_source_modules[att_type](attributeconf)
                self._attribute_sources.append(attribute_source)
            except KeyError:
                raise AbfabIDPError('Unknown attribute source [{0}]'.format(att_type))
            except AttributeSourceError as e:
                raise AbfabIDPError(
                    'Could not instantiate the attribute source: {0}'.format(e))

        # create the attribute filters
        self._attribute_filters = []
        for attribute_filter_conf in idpconf.get('attribute_filters', []):
            filter_type = attribute_filter_conf.get('type', 'Notype')
            try:
                attribute_filter = attribute_filter_modules[filter_type](attribute_filter_conf)
                self._attribute_filters.append(attribute_filter)
            except AttributeFilterError as e:
                raise AbfabIDPError('Could not instantiate the attribute filter: {0}'.format(e))

    def _retrieve_attributes(self, username, acceptor_name):
        """ Gets the attributes of the user stored in different sources

            :param username: name of the user
            :param acceptor_name: name of the RP asking for the attributes
            :returns: List of tuples (attr_name, attr_name_format, [values])
        """
        attributemap = collections.defaultdict(list)
        for attribute_source in self._attribute_sources:
            try:
                for name, namefmt, values in attribute_source.retrieve_attributes(username,
                                                                                  acceptor_name):
                    attributemap[(name, namefmt)] += values
            except AttributeSourceError as e:
                radiusd.radlog(radiusd.L_ERR,
                               'Error in AttributeSource [{0}]: {1}. Omitting results from this '
                               'source.'.format(attribute_source.name, e))

        # build a linear version of the attributemap
        return [(name, namefmt, values) for (name, namefmt), values in attributemap.items()]

    def _build_assertion(self, attributes):
        now = datetime.datetime.utcnow()
        later = now + datetime.timedelta(minutes=self._lifetime)

        # build the base Assertion object
        return saml.Assertion(
            issue_instant=now.strftime('%Y-%m-%dT%H:%M:%SZ'),
            id=str(uuid.uuid4()),
            version='2.0',
            issuer=saml.Issuer(text=self._issuer),
            subject=saml.Subject(
                name_id=saml.NameID(text=str(uuid.uuid4()), format=saml.NAMEID_FORMAT_TRANSIENT),
                subject_confirmation=saml.SubjectConfirmation(
                    method='urn:ietf:params:abfab:cm:user')),
            conditions=saml.Conditions(not_before=now.strftime('%Y-%m-%dT%H:%M:%SZ'),
                                       not_on_or_after=later.strftime('%Y-%m-%dT%H:%M:%SZ')),
            attribute_statement=self._attributes_to_statement(attributes))

    def post_auth(self, p):
        """ Implements the functionality associated to the post-auth callback.
            In particular, it generates the SAML Assertion to be sent with the
            Access-Accept

            :param p: List of RADIUS attribute tuples (name, value)
            :returns: FreeRadius status code
        """
        # Get the User-Name from the Access-Request and calculate the Realm
        username = self._get_radius_attribute(p, 'User-Name')
        if not username:
            radiusd.radlog(radiusd.L_WARN, 'No User-Name attribute found. Omit SAML generation.')
            return radiusd.RLM_MODULE_NOOP, None, None

        # Get the RP name
        acceptor_name = self._get_acceptor_name(p)
        radiusd.radlog(radiusd.L_INFO,
                       '[{0}] requested authentication of user [{1}]'.format(acceptor_name,
                                                                             username))

        # get the end user attributes
        attributes = self._retrieve_attributes(username, acceptor_name)

        # perform attribute filters
        attributes = self._perform_filters(attributes, rp_name=acceptor_name, idp_name='')

        radiusd.radlog(radiusd.L_INFO,
                       'Attributes after filtering: [{0}]'.format(
                           ', '.join([x[0] for x in attributes])))

        # generate the assertion
        assertion = self._build_assertion(attributes)

        # generate the reply
        reply = self._assertion_to_reply(assertion)

        return radiusd.RLM_MODULE_OK, reply, None


# FreeRadius' module callbacks
def instantiate(p):
    global global_idp
    """ Instantiates the module. This just tries to parse the configuration
    """
    try:
        with open('/etc/abfab_idp.conf', 'r') as f:
            configuration = json.loads(f.read())
            global_idp = AbfabIDP(configuration)
    except IOError as e:
        radiusd.radlog(radiusd.L_ERR,
                       '/etc/abfab_idp.conf JSON configuration file missing: {0}'.format(e))
        return -1
    except ValueError as e:
        radiusd.radlog(radiusd.L_ERR,
                       '/etc/abfab_idp.conf JSON configuration file error: {0}'.format(e))
        return -1
    except AbfabIDPError as e:
        radiusd.radlog(radiusd.L_ERR, 'Error instantiating ABFAB IDP: {0}'.format(e))
        return -1
    except Exception as e:
        radiusd.radlog(radiusd.L_ERR, 'ABFAB IDP uncaught exception: {0}'.format(e))
        traceback.print_exc(file=sys.stdout)
        return -1
    return 0


def post_auth(p):
    try:
        return global_idp.post_auth(p)
    except AbfabIDPError as e:
        radiusd.radlog(radiusd.L_ERR, 'Error instantiating ABFAB IDP: {0}'.format(e))
        return radiusd.RLM_MODULE_FAIL
    except Exception as e:
        radiusd.radlog(radiusd.L_ERR, 'ABFAB IDP uncaught exception: {0}'.format(e))
        traceback.print_exc(file=sys.stdout)
        return radiusd.RLM_MODULE_FAIL
