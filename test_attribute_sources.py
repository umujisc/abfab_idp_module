# -*- coding: utf-8 -*-
# !/usr/bin/python

import os
import sqlite3
import unittest

from attribute_sources import (AttributeSourceError, SQLiteAttributeSource, LDAPAttributeSource,
                               StaticAttributeSource)

__author__ = "Alejandro Pérez <alex@um.es>"


class TestSQLiteAttributeSource(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        try:
            os.unlink('/tmp/dummy_db.db')
        except OSError:
            pass
        # creates dummy sqlite3 db
        conn = sqlite3.connect('/tmp/dummy_db.db')
        cursor = conn.cursor()
        cursor.execute('''
            CREATE TABLE attributes (username TEXT NOT NULL, name TEXT NOT NULL,
                                     name_format TEXT NOT NULL, value TEXT NOT NULL,
                                     PRIMARY KEY(username, name))''')
        cursor.execute(
            'INSERT INTO attributes VALUES(\'testuser1\', \'attr1\', \'format1\', \'value1\')')
        cursor.execute(
            'INSERT INTO attributes VALUES(\'testuser1\', \'attr2\', \'format1\', \'value1\')')
        cursor.execute(
            'INSERT INTO attributes VALUES(\'testuser1\', \'attr3\', \'format1\', \'value1\')')
        cursor.execute(
            'INSERT INTO attributes VALUES(\'testuser2\', \'attr1\', \'format1\', \'value1\')')
        cursor.execute(
            'INSERT INTO attributes VALUES(\'testuser2\', \'attr2\', \'format1\', \'value1\')')
        conn.commit()

    @classmethod
    def tearDownClass(cls):
        os.unlink('/tmp/dummy_db.db')

    def setUp(self):
        self.rp_name = 'service@host'
        self.conf = {
            'sqlite_db': '/tmp/dummy_db.db',
        }

    def test_emtpy_conf(self):
        with self.assertRaises(AttributeSourceError):
            SQLiteAttributeSource({})

    def test_file_not_exists(self):
        conf = {'sqlite_db': 'nofile.sqlite3'}
        with self.assertRaises(AttributeSourceError):
            SQLiteAttributeSource(conf)

    def test_username_none(self):
        attsource = SQLiteAttributeSource(self.conf)
        attsource.retrieve_attributes(None, self.rp_name)

    def test_rp_name_none(self):
        attsource = SQLiteAttributeSource(self.conf)
        attsource.retrieve_attributes('testuser1', None)


class TestLDAPAttributeSource(unittest.TestCase):
    def setUp(self):
        self.rp_name = 'service@host'
        self.conf = {
            'ldap_uri': "ldap://ldap.um.es:389",
            'ldap_basedn': "dc=usuarios,dc=um,dc=es",
            'ldap_mapping': "(mail=%u)",
        }
        self.username = 'alex@um.es'

    def test_emtpy_conf(self):
        with self.assertRaises(AttributeSourceError):
            LDAPAttributeSource({})

    def test_username_none(self):
        with self.assertRaises(TypeError):
            attsource = LDAPAttributeSource(self.conf)
            attsource.retrieve_attributes(None, self.rp_name)

    def test_rp_name_none(self):
        attsource = LDAPAttributeSource(self.conf)
        attsource.retrieve_attributes(self.username, None)


class TestStaticAttributeSource(unittest.TestCase):
    def setUp(self):
        self.rp_name = 'service@host'
        self.conf = {
            'static_name': "attribute1",
            'static_name_format': "format1",
            'static_value': "value1"
        }
        self.username = 'alex@um.es'

    def test_emtpy_conf(self):
        with self.assertRaises(AttributeSourceError):
            StaticAttributeSource({})

    def test_username_none(self):
        attsource = StaticAttributeSource(self.conf)
        result = attsource.retrieve_attributes(None, self.rp_name)
        self.assertTrue(result == [('attribute1', 'format1', ['value1'])])

    def test_rp_name_none(self):
        attsource = StaticAttributeSource(self.conf)
        attsource.retrieve_attributes(self.username, None)

    def test_all(self):
        attsource = StaticAttributeSource(self.conf)
        result = attsource.retrieve_attributes(self.username, self.rp_name)
        self.assertTrue(result == [('attribute1', 'format1', ['value1'])])


if __name__ == '__main__':
    unittest.main()
