# -*- coding: utf-8 -*-
# !/usr/bin/python

import unittest

import radiusd
from abfab_idp import AbfabIDP

__author__ = "Alejandro Pérez <alex@um.es>"


class TestAbfabIDP(unittest.TestCase):
    def setUp(self):
        self.conf = {
            "assertion_builder": {
                "type": "pysaml",
                "issuer": "http://myradius.org/",
                "lifetime": 600,
            },
            "attribute_sources": [
                {
                    "type": "static",
                    "static_name": "testattr1",
                    "static_value": "val1"
                }
            ]
        }

    def test_emtpy_conf(self):
        AbfabIDP({})

    def test_no_username(self):
        idp = AbfabIDP(self.conf)
        code, attributes, _ = idp.post_auth([
            ('RADATTR1', 'whatever'),
            ('RADATTR2', 'whatever'),
            ('RADATTR3', 'whatever'),
            ('RADATTR4', 'whatever'),
        ])
        self.assertEqual(code, radiusd.RLM_MODULE_NOOP)

    def test_valid1(self):
        idp = AbfabIDP(self.conf)
        code, attributes, _ = idp.post_auth([
            ('User-Name', 'alex@test.org'),
            ('RADATTR1', 'whatever'),
            ('RADATTR2', 'whatever'),
            ('RADATTR3', 'whatever'),
            ('RADATTR4', 'whatever'),
        ])
        self.assertEqual(code, radiusd.RLM_MODULE_OK)


if __name__ == '__main__':
    unittest.main()
