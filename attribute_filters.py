# -*- coding: utf-8 -*-
# !/usr/bin/python

""" Python module attribute_filters file """

import re

import radiusd

from saml2 import saml

__author__ = "Alejandro Pérez <alex@um.es>"


class AttributeFilterError(Exception):
    pass


class AttributeFilter(object):
    def __init__(self, configuration):
        self._rp_name = configuration.get('rp_name', '.*')
        self._idp_name = configuration.get('idp_name', '.*')

    def filter_attributes(self, attributes, rp_name='', idp_name=''):
        """ Filters attributes based on the peer_name

            :param attributes: List of tuples (name, name_format, [values])
            :param rp_name: Textual name of the RP
            :param idp_name: Textual name of the IDP
            :returns: Filtered list of attribute tuples
        """
        if not (re.match(self._rp_name, rp_name) and re.match(self._idp_name, idp_name)):
            return attributes

        return self._filter_attributes(attributes)

    def _filter_attributes(self, attributes):
        """ Filters attributes

            :param attributes: List of tuples (name, name_format, [values])
            :returns: Filtered list of attribute tuples
        """
        raise NotImplementedError


class BlacklistAttributeFilter(AttributeFilter):
    def __init__(self, configuration):
        AttributeFilter.__init__(self, configuration)
        self._exclude = configuration.get('exclude', [])
        self._include = configuration.get('include', [])

    def _filter_attributes(self, attributes):
        excluded = []
        included = []
        for exclude in self._exclude:
            excluded += [x for x in attributes if re.match(exclude, x[0])]
        for include in self._include:
            included += [x for x in attributes if re.match(include, x[0])]
        accepted_attributes = [x for x in attributes if x not in excluded or x in included]
        return accepted_attributes


class TemplateAttributeFilter(AttributeFilter):
    def __init__(self, configuration):
        AttributeFilter.__init__(self, configuration)
        self._source = configuration.get('source', '')
        self._destination = configuration.get('destination', 'new_template_attribute')
        self._re = configuration.get('re', '')
        self._template = configuration.get('template', '')

    def _filter_attributes(self, attributes):
        new_attributes = []
        for name, namefmt, values in attributes:
            if name != self._source:
                continue

            if len(values) == 0:
                continue

            if len(values) > 1:
                radiusd.radlog(radiusd.L_WARN,
                               'Template transformation only takes into account the'
                               ' first value of a multivalued attribute')
            value = values[0]
            match = re.match(self._re, value)
            if match is None:
                radiusd.radlog(radiusd.L_WARN,
                               'Attribute {0} does not match re {1}: {2}'.format(name, self._re,
                                                                                 value))
                continue
            try:
                new_value = self._template.format(*match.groups())
                new_attributes.append((self._destination, namefmt, [new_value]))
            except IndexError:
                radiusd.radlog(radiusd.L_WARN,
                               'Failed to build attribute "{0}" from template'.format(
                                   self._destination))
                continue
        return attributes + new_attributes


class CombineAttributeFilter(AttributeFilter):
    def __init__(self, configuration):
        AttributeFilter.__init__(self, configuration)
        self._sources = configuration.get('sources', [])
        self._destination = configuration.get('destination', 'new_combine_attribute')
        self._template = configuration.get('template', '')

    def _filter_attributes(self, attributes):
        collected_values = []
        for source in self._sources:
            for name, namefmt, values in attributes:
                if name != source:
                    continue

                if len(values) == 0:
                    continue

                if len(values) > 1:
                    radiusd.radlog(radiusd.L_WARN,
                                   'Combination transformation only takes into account the'
                                   ' first value of a multivalued attribute')

                collected_values.append(values[0])
        if not collected_values:
            radiusd.radlog(radiusd.L_WARN, 'No source attributes has been found!')
            return attributes
        try:
            new_value = self._template.format(*collected_values)
            new_attribute = self._destination, saml.NAME_FORMAT_UNSPECIFIED, [new_value]
            return attributes + [new_attribute]
        except IndexError as ex:
            radiusd.radlog(radiusd.L_WARN,
                           'Failed to build attribute "{0}" from combination: {1}'.format(
                               self._destination, ex))
            return attributes


attribute_filter_modules = {
    'blacklist': BlacklistAttributeFilter,
    'template': TemplateAttributeFilter,
    'combine': CombineAttributeFilter,
}
